build:update lualatex bibliography lualatex lualatex 
	@# Fiks alt som er forkrav og så:
	# Endre filnavn på filen
	cd src/ && mv kokebok.pdf ../KokebokenTilFredrik.pdf

build_pdflatex:
	#Update all sorterteOppskrifter* to include new recipies
	cd src/ && ./update.sh
	cd src/ && pdflatex -jobname=KokebokenTilFredrik kokebok.tex

doublebuild:
	#Update all sorterteOppskrifter* to include new recipies
	cd src/ && ./update.sh
	#Build a PDF with pdflatex
	cd src/ && pdflatex -jobname=KokebokenTilFredrik kokebok.tex
	#And again for annotations and references
	cd src/ && pdflatex -jobname=KokebokenTilFredrik kokebok.tex

send-to-web:
	scp KokebokenTilFredrik.pdf fredrik@0v.no:/var/www/fredriklindseth/_site/assets/kokebok.pdf
	scp KokebokenTilFredrik.epub fredrik@0v.no:/var/www/fredriklindseth/_site/assets/kokebok.epub

update:
	#Update all sorterteOppskrifter* to include new recipies
	cd src/ && ./update.sh
bibliography:
	cd src/ && bibtex kokebok

lualatex:
	cd src/ && lualatex kokebok.tex

all: build run

lint:
	#use chktex to lint, it expands the /includes
	cd src/ && chktex -q -n 6 kokebok.tex > ../chktex.log

run:
	# Using evince/Gnome PFD reader for PDF viewing
	evince src/KokebokenTilFredrik.pdf &

update_dirty:
	cd src/ && ./update.sh

#oppdater:
#	basename -s .tex oppskrifter/*.tex | sed 's/^/\\include{oppskrifter\//' | sed 's/$/}/' > sorterteOppskrifter.tex
	#todo \ foran include kommer ikke med i resultatet
	#todo det er noe gale med uttrykket "sed 's/$/}/'" } kommer ikke med og får feil:
		#sed: -e expression #1, char 4: unterminated `s' command
		#Makefile:13: recipe for target 'oppdater' failed

food: all
	#alias target

ebook: ebok
	#alias target

ebok:
	#Update all sorterteOppskrifter* to include new recipies
	cd src/ && ./update.sh

	#Expanding the main.tex to include all \includes in one big file
	cd src/ && perl latexpand kokebok.tex > kokebok-expanded.tex

	#Convertering the main.tex to epub
	cd src/ && pandoc -f latex -t epub -o KokebokenTilFredrik.epub kokebok-expanded.tex
	cd src/ && mv KokebokenTilFredrik.epub ../KokebokenTilFredrik.epub
	cd src/ && rm kokebok-expanded.tex

clean:
	@#@ to ignore the echo of the command
	@#-f to will FORCE and not output any error
	@rm -f src/*.aux # LaTeX auxiliary file
	@rm -f src/*.toc # auxiliary file for the Table of Contents
	@rm -f src/*.lof # auxiliary file for the List of Figures
	@rm -f src/*.lot # auxiliary file for the List of Tables
	@rm -f src/*.idx # makeidx / index
	@rm -f src/*.ilg # makeidx / index
	@rm -f src/*.ind # makeidx / index
	@rm -f src/*.log # .log: Log file for the compilation
	@rm -f src/*.out # information about the sectional units that will be used to write the outlines.
	@rm -f src/*.fls # Core latex/pdflatex auxiliary file
	@rm -f src/*.blg # Bibliography (BiBTeX) log
	@rm -f src/*.synctex.gz # latexmake build tool auxillary
	@rm -f src/*.bbl # Bibliography
	@rm -f src/*.xdy # Glossary /xindy
	@rm -f src/*.alg # Glossary /xindy
	@rm -f src/*.glg # Glossary /xindy
	@rm -f src/*.acr # Glossary 
	@rm -f src/*.acn # Glossary 
	@rm -f src/*.glo # Glossary
	@rm -f src/*.gls # Glossary
	@rm -f src/*.glsdefs # Glossary
	@rm -f src/*.fdb_latexmk # latexmake build tool auxillary
	@rm -f src/*.brf # hyperref
	@rm -f src/*.bak # hyperref
	@rm -f src/*.bcf
	@rm -f src/*.run.xml

	@# Remove aux from subfolders
	@cd src/oppskrifter && rm -f *.aux
	@cd src/oppskrifter/brygg && rm -f *.aux
	@cd src/oppskrifter/desert && rm -f *.aux

	@rm -f src/kokebok-expanded.tex
	@rm -f src/kokebok-expanded.pdf
	@rm -f chktex.log
	@rm -f src/main.pdf
	@rm -f src/kokebok.pdf
	@rm -f KokebokenTilFredrik.pdf
	@rm -f /home/fredrik/Documents/Kokebok/tex/KokebokenTilFredrik.epub
	