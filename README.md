# Kokeboken til Fredrik

![travis-ci.org/FredrikLindseth/kokebok](https://travis-ci.org/FredrikLindseth/kokebok.svg?branch=master)

![preview](preview.png)

Dette er LATEXversjonen av oppskriftene vi har samlet sammen i husholdning og lager med jevne og ujevne mellomrom. Kokeboken finnes i PDFform og i EPUB, for eboklesere.

Den nyeste versjonen finnes i listen over utgivelser [her](https://gitlab.com/FredrikLindseth/kokebok/-/tags) eller som [FredrikSinKokebok.pdf](KokebokenTilFredrik.pdf) i rotmappen når man bygger den lokalt. Kokeboken bygges fra [kokebox.tex](src/kokebok.tex) som er hovedlatexfilen.

Alle oppskriftene ligger i egne filer under [Oppskrifter](oppskrifter). Den enkleste måten å lage en oppdatert utgave denne PDFen er å kjøre kommandoen `make food`.

## Bygge kokeboken

For å lage en PDF av kokeboken lokalt kjør kommandoen `make food` (antar du at har Make installert). Kokeboken krever at man har lualatex installert og den enkleste måten å sørge for at man har alt det nødvendige er å installere pakken `texlive-full` / hele texlive

## Legge til nye oppskrifter

For å legge til nye oppskrifter er det enklest å kopiere [oppskriftsmal.tex](src/oppskrifter/oppskriftsmal.tex) og tilpasse den oppskiften for så å lagre den enten under /oppskrifter/ eller en passende undermappe.

## Oppdatering av oppskriftslisten

`sorterteOppskrifter.tex`, `sorterteDeserter.tex` og `sorterteBrygg.tex` er filer som inneholder oppskriftene fra undermappene som `/oppskrifter/desert` og `/oppskrifter/brygg`.

Disse sorterte-filene bygges automatisk når man kjører `make food`, eller manuellt ved å kjøre `make update-dirty`. Disse instruksfilene holder oppskriftene i alfabetisk orden i kokeboken.

Ved å kjøre `make food` vil listen over inkluderte oppskrifter bli oppdatert, en PDF satt sammen for videre å bli åpnet i evince (gnome PDF viewer).

## Bygging med Travis

Kokeboken kan bygges automatisk med Travis CI med filen [.travis.yml](./.travis.yml) i rotmappen. Dette er deaktivert for dette repoet, men det skal virke Helt Fint [TM]

## Ebokutgave

Ved å utføre kommandoen `make ebok` vil en EPUB-fil ble generert.

**NB** Denne eboken har noen feil som kommer av konverteringen fra latex til EPUB, som at alle brøker vises som `\frac{1}{2}`, sist oppdatert dato som `\today` og merkelapper som `\label{lussekatter}`.

## English

This is my kitchen cookbook made in LATEX. The whole thing is written in Norwegian , but the output can be viewed [here](https://gitlab.com/FredrikLindseth/kokebok/-/tags)
